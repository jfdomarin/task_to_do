# Task Unit test

1. Copy the code from here: https://gist.github.com/js-mentoring/ba9c4eba7b3d24ed256f17b2464e8b80.
2. Add code coverage c8.
3. Cover all the methods with unit tests.
4. Add mochawesome as a reporter.
